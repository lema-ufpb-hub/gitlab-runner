# Executor GitLab CI-CD

Projeto em docker para implantação de executor GitLab CI/CD.

## Cadastrar Executor de Grupo

Exemplo de executor cadastrado:

![image](./img/example.png)

## Provisionar containers

No host de automação:

```bash
git clone https://gitlab.com/lema-ufpb-hub/gitlab-runner.git ~/.gitlab-runner
cd ~/.gitlab-runner
mv sample.env .env
# Editar nome do executor e o token
nano .env
docker-compose up -d
docker-compose ps
```
